Linux dynamically assigned ports
================================

So you want to assign your application a ``high`` port number, maybe
you do some testing (and you'll launch many instances of servers), maybe you
want to set some service on unusual port (which is ok in some cases).

What you don't want is your selected port to clash with the range of ports
that are automatically assigned to outgoing connections. This range is
sadly OS dependent, IANA defines it as: 49152--65535, however linux
chooses different range, that can be read by: ``cat /proc/sys/net/ipv4/ip_local_port_range ``.

For more information `see this SO post <http://stackoverflow.com/a/924337/7918>`__.