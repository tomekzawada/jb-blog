W530 battery life
=================

After about a year of usage maximal charge of my battery dropped about 40%,
which is way too much. On Windows there are some settings that "enhance
battery lifespan".

.. note::

    It seems that to enhance lifespan of your batteries you'll need to:

    * Charge it for longer periods (ideally allways to full charge)
    * Don't charge it to 100%, set full charge to 90-something%

There are no generic tools to configure such behaviour --- as it all sits
inside BIOS. `This question
<http://askubuntu.com/questions/34452/how-can-i-limit-battery-charging-to-80-capacity>`_
on ubuntu SO helped me to find solution, but nevertheless it didnt work on
my W530 laptop.

On newer thinkpats you'll need to use: `tcpapi-bat
<https://github.com/teleshoes/tpacpi-bat>`_ (you might install it from
`this PPA <https://launchpad.net/~morgwai/+archive/tpbat>`_ (it's for ubuntu
but also works on Debian.

After instal you'll need to insert following into `/etc/rclocal.d` (it
will be called after startup):

.. code-block:: bash

    tpacpi-bat -s ST 0 80
    tpacpi-bat -s SP 0 90

It will do the following: battery will stop chaging after charging to ``90%`` (
of current maximal charge) and battery will start charging only after it is
discharged to ``80%`` (which will cause that you will charge it for 10% capacity
or greater).

It would be great it I could (for example) inhibit charging for (let's say) 15
minutes after connectiong to AC, as I often connect the computer and then decide
I need to go somewhere else, which is not healthy for battery.
