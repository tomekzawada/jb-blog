Disabling Optimus on Debian and using only Nvidia Graphics card
===============================================================

I had some problems with bumblebee and optirun command --- it
just suddenly stopped working.

So here is how to switch your computer to use nvidia on debian,
and use nvidia proprietary drivers

1. Remove ``bumblebee``
2. Install Nvidia proprietary drives (https://wiki.debian.org/NvidiaGraphicsDrivers).

   .. note::

       Remember to configure ``xorg.conf`` precisely as in
       the instructions.

3. Restart. Go to bios and switch off integrated graphics card.

If you restart your computer before step 3, most probably your
X will not be usable.
