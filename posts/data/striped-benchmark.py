import subprocess, pathlib, re

BENCHMARK_PREPARE = ""

BENCHMARK_TEMPLATE = (
    "sysbench fileio --file-total-size=128GB --file-test-mode=rndrw "
    "--file-fsync-freq=10 --threads=8 --file-block-size={} run"
)

benchmarks = [
  (str(n), BENCHMARK_TEMPLATE.format(int(2**n))) for n in [15, 16, 17, 18, 19, 20]
]

logical_volume_name = "tested-volume"

lv_size_gb = 256

num_stripes = 2

stripes_to_check = [64]

mount_point = "/tmp/test-stripe"
volume_group = "your volume group"

mount_options = "defaults,noatime"

dev_mapper = "/dev/{}/{}".format(volume_group, logical_volume_name)

report_dir = pathlib.Path("/tmp/reports")

pathlib.Path(mount_point).mkdir(parents=True, exist_ok=True)


def check_call(cmd, **kwargs):
  print(cmd)
  subprocess.check_call(re.split("\s+", cmd), **kwargs)
  print("Done")


def check_output(cmd, **kwargs):
  return subprocess.check_output(re.split("\s+", cmd), **kwargs)


def prepare_fileystem(stripe_kb: int, ):
  stripes = ""
  if stripe_kb > 0:
    stripes = "-d su={stripe_kb}k,sw={stripe_num}".format(stripe_kb=stripe_kb, stripe_num=num_stripes)
  cmd = "mkfs.xfs {stripes} {dev_mapper}"
  check_call(cmd.format(
    stripe_kb=stripe_kb,
    dev_mapper=dev_mapper,
    stripes=stripes
  ))


def prepare_lv(stripe_size):
  stripes = ""
  if stripe_size > 0:
    stripes = "--stripes {} --stripesize {}".format(
      num_stripes, stripe_size
    )
  cmd = "lvcreate -W y --yes {stripes} --size {size}G -n {logical_volume_name} {volume_group}".format(
    stripes=stripes,
    size=lv_size_gb,
    volume_group=volume_group,
    logical_volume_name=logical_volume_name
  )
  check_call(cmd)


def mount():
  cmd = "mount -t xfs -o {options} {dev_mapper} {mount_point}".format(
    options=mount_options,
    dev_mapper=dev_mapper,
    mount_point=mount_point
  )
  check_call(cmd)


def umount():
  check_call("umount {}".format(mount_point))


def remove_lv():
  check_call("lvremove -f {volume_group}/{logical_volume_name}".format(
    volume_group=volume_group,
    logical_volume_name=logical_volume_name
  ))


def prepare_bench():
  subprocess.check_call(BENCHMARK_PREPARE.split(" "), cwd=mount_point)


def run_bench(stripe_size, benchmark_name, benchmark_command):
  stripe_report = report_dir / str(stripe_size)
  stripe_report.mkdir(parents=True, exist_ok=True)
  stripe_file = stripe_report / benchmark_name
  output = check_output(benchmark_command, cwd=mount_point)
  stripe_file.write_bytes(output)


def do_stripe(stripe_size):
  try:
    prepare_lv(stripe_size)
    prepare_fileystem(stripe_size)
    mount()
    prepare_bench()
    for bench_name, bench_cmd in benchmarks:
      print(stripe_size, bench_name)
      run_bench(stripe_size, bench_name, bench_cmd)
  finally:
    try:
      umount()
    finally:
      remove_lv()


def do_stuff():
  for stripe in stripes_to_check:
    do_stripe(stripe)


do_stuff()