Cheap and fast way to create your own Maven repository on S3
============================================================

Maven is de-facto standard build system for Java apps, it's a nice and mature piece of software.

To meaningfully use it you need to host your own maven repository --- that allows you and
your teammates to download and release software artifacts you use. Most of these repositories
is written in Java, and is not very light --- most probably you'd need to have a dedicated VPS,
which is OK --- unless you use this repository for hobby project that is in maintenenance mode
and you touch it once a year.

Cheap and easy way to host Maven repositories is to use S3, there you pay only for storage, and
transfer, which is preatty cheap.

Simple guide is here: https://github.com/spring-projects/aws-maven.



