Meltdown and spectre bugs
=========================

Media coverage for meltdown and spectre bugs is pretty awful. So here is my
recap.

In Q/A style.

.. note::

    This is not really for hackers, there is very nice explanation of these
    bugs in Google Zero blogs, read it.

    This page is for people who want more of a layman description, that is
    close to the truth, without being too pessimistic nor too optimistic
    about the impact of these vulnerabilities.

What is the fuss about
######################

Here I will describe how meltdown and spectre works, there will be a "what to
do" section later.

What is process isolation
-------------------------

Let's say you have a computer, and you have opened a browser and a password
manager.

One of the (more important roles) of your operating system (Windows/Linux/Whatnot)
is to make sure that these programs can not read each others memory.

So lets say you are logging to facebook, and your password manager contains
a password for your bank, you wouldn't want your browser to be able to know
password to your bank unless you paste it yourself.
Operating system makes sure that this is not possible, or at least it did that until last week.

You also don't want "facebook" tab to know password you are entering on your
"bank" tab (this is enforced by the browser though --- still by the OS in case
of ``google-chrome``).

What is kernel / userland isolation
-----------------------------------

There are two kinds of programs in your computer:

* programs that are part of the kernel (the operating system itself)
* the rest are so called userspace programs.

Kernel code has much more privileges than userspace programs. And we don't want
to enable userland programs to be able to access kernel memory. Operating system
makes sure that this is not possible, or at least it did that until last week.

How these attacks work (background knowledge)
---------------------------------------------

These attacks use so called 'side channel attacks' and 'speculative execution'
to break both kernel and process isolation.


CPU Caches
**********

CPU is another name for processor.

You might thing that you have two kinds of memory in your computer, one is
"slow" (your drive), and one is "fast" (RAM memory).

This is wrong, you have at least three kinds of memory:

* CPU Cache (it is "fast")
* RAM Memory (it is "slow")
* Disks ("very slow")

Your processor (CPU) copies parts of ram into cache to speed-up execution.

What is speculative execution
*****************************

Imagine you are a railroad junction operator in 1800, you hear that a train is
coming but you have no idea to what side of the junction train will go, you
can do two things:

1. Stop the train. But trains in 1800 took a lot of time to brake and speed up!
2. Assume that train will go, let's say, right. If train indeed wanted to go
   right you just saved everybody a lot of time! If train wanted to go left,
   you now need to stop the train, put it in reverse, flip the junction then
   restart the train on the other path.

Note: this description is taken from here: https://stackoverflow.com/questions/11227809/why-is-it-faster-to-process-a-sorted-array-than-an-unsorted-array
thanks John Skeet!

CPU works somewhat like a train --- they also take a long time to get up to speed,
and we also have junction points, when processor needs to make some decision,
sometimes this decision is based on memory that is not available (not in CPU cache
this information might be on RAM or on disk).

CPU's have some specialized hardware called a "branch predictor" that can try
to guess which side of the junction it will take, if they are wrong, CPU is
supposed to erase all trace of "wrong junction", and then restart.

Side channel attacks
********************

This is a broad category of attacks on computer system that use some kind
of extra information provided by the system ("side channel").

In most cases his "side channel" is timing information (in other cases
"side channel" might be power consumption of chip embedded in your credit card)

Let's have a simple example, you have a login page on a webpage, user has a
password that is ``psswrd``. And password checking algorithm is as follows

* If first letter of password and user input does not match, return that
  passwords don't match.
* If second letter of password and user input does not match return that
  passwords don't match.
* ... you get it.

.. note::

    No sane webpage would use this algorithm for a variety of reasons!

Now this password scheme is very easy to break. First attacker tries
passwords that look like ``aa``, ``ba``, ``ca``, ..., ``pa``, ..., ``za``.

When checking ``aa`` password website will make **one check** whether ``a`` equals ``p``
(first letters of both real password and input attacker provided). When it's checking
``pa`` it will need to make **two checks** first to compare ``p`` and ``p`` and then
to compare ``a`` and ``s``. Two checks take more time than one.

Website will take slightly longer to check ``pa`` password than all others,
as computer will make need to check two letters and for all other passwords
it will only need to check a single letter.

In this way attacker has established that first letter of password is ``p``.

Breaking password this way is, much, much, easier than guessing all possible passwords.


How these attacks work
----------------------

I'll explain a single one of these attacks, one codenamed: "Meltdown".

You want to learn whether a single bit system memory is one or zero. Let's say
that this bit is named ``unknown_bit``.

You craft a program that works as follows:

1. You create a ``variable``.
2. You make sure that CPU cache of your processor is empty.
3. You read ``unknown_bit``. You have no right to read this memory, but CPU
   doesn't know if you can or can't read this memory. It will try
   'speculative execution' assuming that you have this right.
4. You perform some operation on ``unknown_bit`` which will result in reading
   ``variable`` if this bit is zero, and reading something else if it is one.

   Now depending on whether ``unknown_bit`` is in zero or one, ``variable``
   either is in CPU cache or not.
5. Some time later CPU discovers that you couldn't have read ``unknown_bit`` and
   restores state of the CPU, from step 2.

   The problem is (and this is a bug) that CPU doesn't clear the cache.

6. Now you measure **how long it takes for you to read ``variable``**, if the
   time is required for the read is short, it means that ``variable`` is in cache!
   And if this time is long this means that ``variable`` was not in cache.
   This way you can guess whether ``unknown_bit`` was zero or one.

Spectre works in a similar way.

What should I do
################


What is broken
--------------

Following things are broken:

* One program to read memory for other programs;
* A user program to read memory of kernel;


Do these vulnerabilities have a patch
-------------------------------------

1. There is a patch that disallows user to read kernel memory.
2. There is no patch that disallows one program to read memory of other program.

   **There probably will never be such patch**.

3. Most probably CPU manufacturers won't be able to patch this using microcode
   update

What is microcode
*****************

CPU is a electronic device --- that is most of it's logic is hardwired on a
piece of silicon (this piece of silicon is the processor). However in modern
CPU's some operations are done using a microcode --- that is are small programs
that execute on CPU. CPU manufacturers can fix some bugs in CPU's remotely
by issuing "microcode update" --- that is by fixing these small programs.

What do I do
------------

Ultimately you wait for AMD or Intel to release a CPU without this bug, and
buy one.

What do I do in the meantime
----------------------------

You keep your computer up to date by installing all updates for every program
you own! Especially:

* You keep your windows/linux up to date!
* You keep your web browser up to date!

There are some mitigation techniques that might be employed **by each of the
programs** to make these bugs harder to exploit.

The same exploits are possible on Android/IPhone devices, so keep your device
up to date, and if it does not receive updates, then you are out of luck  and
probably will need to replace.

Is my computer hackable right now
---------------------------------

No, ... and somewhat yes. It's complicated.

I believe following things are true right now (2018-01-05):

* Noone is actively using this exploit to target personal computers.
* These exploits require the attacker to run programs on your computer. Which
  usually already means "game over" for security.
* These attacks only allow attacker to **read** memory. They can't alter it.

  However reading of memory of other programs can, for example, allow attacker to:

  * read your passwords stored in password manager;
  * read private keys of your certificate stored on hard drive;

Worst case scenario, right now, is:

* You visit a malicious webpage.
* This webpage is able to read memory of your computer and send it some nasty
  people.

Can they easily read passwords from a password manager, well, again:
yes, and somewhat no.

1. Attacker can read **all memory**, they can't easily "read memory of
   password manager"
2. This exploit is **slow** --- you can read memory at a speed of about 2kb a
   second. Dumping the memory of typical PC can take up to million seconds.

Browser manufacturers, try to harden browser against these exploits, this is
why you should **keep your browser up to date**.

Are some programs especially vulnerable
---------------------------------------

**Yes**, and unfortunately browser is especially vulnerable.

To pull off this attack attacker needs to run a program on your computer,
all modern browsers contain a "JavaScript" engine, and most of the sites use
JavaScript --- JavaScript scripts are small programs that web pages send to your
computer (and that are **executed on your computer**) to e.g. make nice
animations.

Right now it is possible exploit these bugs from JavaScript.

Browser manufacturers, try to harden browser against these exploits, this is
why you should **keep your browser up to date**.

Is this Intel's fault
---------------------

No, and somewhat yes.

1. Most important bug was not found on AMD CPU's. However this bug has a
   patch.
2. Less important bugs are on all CPU's, and they don't have a patch also.
3. There are some rumors that Intel did cuts in verification department in 2013.
   But it's not entirely clear that they would have found this bug. And also
   these are rumors.

References
##########

* https://googleprojectzero.blogspot.se/2018/01/reading-privileged-memory-with-side.html
* https://www.raspberrypi.org/blog/why-raspberry-pi-isnt-vulnerable-to-spectre-or-meltdown/


