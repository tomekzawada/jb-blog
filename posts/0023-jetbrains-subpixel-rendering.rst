How to disable subpixel rendering in Jetbrains products
=======================================================

If you want to disable subpixel rendering (another term is font anitaliasing)
in any JetBrains product (like: Idea, Pycharm and so on), you need to:

1. Go to ``bin`` dir of install folder, find file that ends with ``*vmoptions``
   (``idea.vmoptions``, ``pycharm.vmoptions``).
2. There should be a line
   ``-Dawt.useSystemAAFontSettings=lcd`` in this file, delete this line.
3. Add line: ``-Dprism.lcdtext=false``.

These two lines define properties passed to Java Virtual Machine that control
antialiasing, both of them are undocumented, so they might not work in future
JVM releases (I tested on ``1.8.51``). These JVM settings seem to clash with
either JetBrains settings (that override these) and Gnome settings, so
disabling them makes sense.

