Use git as deployment tool
==========================

Why to do this
--------------

Git people allways tell: "Don't push non-bare repository, "It's a terrible
idea!". Reasons cited are:

1. What if someting is accesing the code on the working copy, you'll change
   the code while program is running.
2. What if code needs to be recompiled.
3. What you'll do if working copy has local changes?
4. What you'll do if the merge fails?

and so on.

However of these concerns only first one is applicable, and easily amended in
some deployments.

.. note::

    Answers to these concerns:

    What if someting is accesing the code on the working copy, you'll change
    the code while program is running.
        This is a valid concern. In my case this was not a problem since --- I
        was the only user of the system and I knew when I could push changes.

    What if code needs to be recompiled.
        Not a problem in case of Python. Also performing some set-up tasks
        (like updating requirements) can be done by hand if it occours rarely
        enough.




Most of the scientific work I do is done in following way: code is developed
on my local computer (with some tests), but it is tested and ran on the cluster.

Often code developed on my laptop had some minor errors, so I had to synchronize
the code often.

My git flow was following:

1. Commit on local computer
2. Push changes
3. SSH to remote maching
4. Fetch changes
5. Merge changes

And if anything was wrong I had to redo all these steps again. This is mind
numbing! Not to mention that it changed 10 sec. change-recompile-test cycle
to 1 minute: change-do-unrelated-stuff-recompile-test cycle.

It would be great if my git-flow would look like:

1. Commit changes on local computer
2. Changes magically are pushed and checked out on repote repository

Since some issues needed to be resolved **on the cluster** I sometimes did changes
on the remote repository and I didn't want them to be lost, and since
using tools like ``rsync`` couldn't easily guarantee that changes on the
server will not be overriden I didn't want to use them.


How to do this
--------------

Anyways it turns out that creating git hooks that are reasonably safe is
quite easy.

So here is the logic:

1. When I commit to local repository changes are automatically pushed to
   remote one
2. When remote repository gets update on checked-out branch changes
   are merged and checked out. If error conditions occour abort merge.
   Following error conditions can be detected:

   1. Working copy is dirty
   2. Merge is not fast forward




