Compress a dir to tar.xz
========================

Everyone is using tar.gz and tar.bz2 formats,
and these compression algorithms (while stable and installed everywhere)
are definetely not state-of-the-art.

Anyways most modern systems have much better compression ratios
(or much less compression/decompression overhead). Namely
``xz`` (better ration) ``lzo`` (much better resource usage).

Anyways since I allways forget syntax to do compression here is
proper command, but today I found this nice tar switch ``-a``
which means: "Try to guess compression format from file extension",
so:

.. code-block:: bash

    tar -caf foo.tar.xz data

will compress to ``xz``, while

.. code-block:: bash

    tar -caf foo.tar.lzo data

will compress to ``lzo``. At least ``tar`` has sane API.

This is nice, but sometimes you'll want to tweak compression ratio
for used compressor --- in this case just use pipes. If you pass
``-`` (if anything else is non-obvious just use ``man``).

.. code-block:: bash

    tar -c data - | xz -9c > data2.tar.xz

