Install node modules without root
=================================

Sometimes you need to install some node modules, but every tutorial says: "Do
``sudo npm install -g something``. I dont like to mix
sudo and downloading some stuff from unsecure location. There is a very easy
way to install such modules in a easy way.

There is how:

1. Download/untar node js. Let's say it is in ``/tmp/node``
2. Create directory named ``"$HOME/.local"`` (or any other really)
3. ``cd /tmp/node``
4. ``configure --prefix="$HOME/.local``
5. ``make && make install``

Now you have a working installation of node in ``$HOME/.local/bin``, add this
directory to ``PATH`` and you'll be able to install modules "globally" to your
home folder, for example:

.. code-block:: bash

    npm install -g coffee-script

and ``coffee`` will be accessible from ``$HOME/.local/bin``.

.. note::

    I used `this <http://tnovelli.net/blog/blog.2011-08-27.node-npm-user-install.html>`_ for inspiration.