CGI on Nginx or how to run man2html on debian
=============================================

I don't enjoy reading manpages, ``man`` has old clunky interface, however do
enjoy reading documentation in html, some good people developen ``man2html``
tool that is a ``CGI`` script that serves man pages in a browser.

This is why in 2015 year I spend part of an evening trying to configure
modern web server to run CGI scripts. This might not be a suprise, that
nginx doesn't serve CGI by default. It can serve ``FastCGI``, which is a
streamlied version of CGI, that unfortunately is incompatible with plain
old CGI scripts.

However there were some other good people that wrote ``fcgiwrap``, which is a
a wrapper that talks FCGI protocol, and then launches plain old ``cgi`` scripts.

So to launch ``man2html`` on Debian, you'll need to:

1. Install required software: ``aptitude install man2html nginx-full fcgiwrap``
2. There is a very good example fcgi config at
   ``/usr/share/doc/fcgiwrap/examples/nginx.conf``, so just copy it to: ``/etc/nginx/fcgiwrap.conf``
3. JUst include ``/etc/nginx/fcgiwrap.conf`` in your server config.



